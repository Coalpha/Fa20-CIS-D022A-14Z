# Fa20.CIS.D022A.14Z

![](misc/footgun.png)

Against my style, I'm going to use tabs since it's what the prof probably wants
to see. Nobody wants to see my garbage three space indentation except that chill
SQL Professor. Also, I like naming my files `.cxx` but I'll put up with `cpp`.
I know, I'm basically a hero at this point for dealing with that. You can email
me my awards. That being said, let's bikeshed a little harder. Which file ending
is truly the superior one? I would argue that `.cc`, while being very nice looking,
can be easily confused with the shell command `cc`, which compiles C instead
of C++. `.cc` does have the advantage of header files being `.hh`, which is good
since it's like h but twice. `.cpp` is the true neutral of the bunch, rather
unremarkable and plain, you'll have nothing to separate you out from the rest of
your 1xer peers. In my correct opinion, `.cxx` is probably the best choice out of
all of the options since Makefiles tend to use `CXX` for compiling C++. Also it has
two "x"es in it which means that you're a certified hacker or gamer just by
virtue of typing the letter twice in succession. And then you have `.c++`. As a
living being, I'm sure you are aware that nearly everything sits within a grey
area of good vs bad. If you're using `.c++`, I'm happy to report that you are one
of those anomalies, resting entirely within the bad category!
